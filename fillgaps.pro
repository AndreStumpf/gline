function fillgaps, img

;; convert line image into a binary response  
;indexNonZero = where(img gt 0)
;nonZeroMin= min(img[indexNonZero])
;binary = img lt nonZeroMin
;binary_orig = binary


; create array to store matches
band_col=(size(img))[1]
band_lin= (size(img))[2]
matches=bytarr(band_col, band_lin)
matches_store=bytarr(band_col, band_lin)
;----------------------------------------------------
;
;
;; horizontal gaps
;miss = [[0b,0b,0b],$  
;       [1b,0b,1b], $  
;       [0b,0b,0b]]
;       
;hit = [[1b,1b,1b],$  
;      [0b,1b,0b], $  
;      [1b,1b,1b]]
;
;matches = matches + (morph_hitormiss(binary, hit, miss))
;
;; vertical gaps
;miss = rotate(miss,1)
;hit = rotate(hit,1)
;matches = matches + (morph_hitormiss(binary, hit, miss))
;
;;add to binary
;binary = binary - matches
;matches_store=matches_store+matches
;matches=bytarr(band_col, band_lin)
;----------------------------------------------------


; diagonal gaps 
miss = [[1b,0b,0b],$  
       [0b,0b,0b], $  
       [0b,0b,1b]]
              
hit = [[0b,1b,1b], $  
      [1b,1b,1b], $  
      [1b,1b,0b]]       

matches= matches + (morph_hitormiss(binary, hit, miss))

; the other diagonal gaps 
miss =rotate(miss,1)
hit = rotate(hit,1)     
matches= matches + (morph_hitormiss(binary, hit, miss))  

;add to binary
matches=matches gt 0
binary = binary - matches
matches_store=matches_store+matches
matches=bytarr(band_col, band_lin)
;----------------------------------------------------


;; asymetric gaps
;miss = [[1b,0b,0b],$  
;       [0b,0b,1b], $  
;       [0b,0b,0b]]
;
;hit = [[0b,1b,1b], $  
;      [1b,1b,0b], $  
;      [1b,1b,1b]]
;
;  for i=0, 7 do begin        
;        missi=rotate(miss, i)
;        hiti=rotate(hit, i)
;        matches= matches + (morph_hitormiss(binary, hiti, missi))
;
;  end
;
;;add to binary
;matches=matches gt 0
;binary = binary - matches
;matches_store=matches_store+matches
;matches=bytarr(band_col, band_lin)
;;----------------------------------------------------
;
;
;; asymetric steps
;miss = [[0b,1b,1b],$  
;       [1b,0b,0b], $  
;       [0b,0b,0b]]
;
;hit = [[1b,0b,0b], $  
;      [0b,1b,1b], $  
;      [1b,1b,1b]]
;  
;  for i=0, 7 do begin
;        missi=rotate(miss, i)
;        hiti=rotate(hit, i)
;        matches= matches + (morph_hitormiss(binary, hiti, missi))  
;  end 
;
;;add to binary
;matches=matches gt 0
;binary = binary - matches
;matches_store=matches_store+matches
;matches=bytarr(band_col, band_lin)
;;----------------------------------------------------
;
;
;; symetric steps
;miss = [[0b,0b,0b],$  
;       [1b,0b,0b], $  
;       [0b,1b,0b]]
;
;hit = [[1b,1b,1b], $  
;      [0b,1b,1b], $  
;      [1b,0b,1b]]
;
;  for i=0, 3 do begin        
;        missi=rotate(miss, i)
;        hiti=rotate(hit, i)
;        matches= matches + (morph_hitormiss(binary, hiti, missi))  
;  end
;
;;add to binary
;matches=matches gt 0
;binary = binary - matches
;matches_store=matches_store+matches
;;----------------------------------------------------
  
return, binary

end