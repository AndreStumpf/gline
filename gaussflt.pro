;+
; NAME: 
;      GAUSSFLT
;
;
; PURPOSE:
;       This funtions generates squared arrays with a 2D gaussian filter kernel
;       based on a given LENGTH in y-direction, the standard deviation SIGMA and
;       the orientation THETA. The main application of the generated filters
;       is detection of dark line in grey level images. The function may create a
;       gaussian matched filter (GMF) or its first order derivate(FDOG)
;
; 
; CATEGORY:
;       Filter
;
;
; CALLING SEQUENCE:
;       g = GAUSFLT(sigma,length,theta,type)
;
;
; INPUTS:
;       SIGMA:    standard deviation of the gaussian funtion (x-direction)
;       LENGTH:   length of the considered neighborhood in y-direction
;       THETA:    orientation of the kernel The value of the last array element in F if
;       /GMF:     calculate GMF
;       /FDOG:    calculate FDOG
;       
;       
; OPTIONAL INPUTS:
;
;         /PLOTG:    plot kernel as a 3D surface
;
; KEYWORD PARAMETERS:
;
; OUTPUTS:
;
;       g:    array with filter kernel
;
; RESTRICTIONS:
;
;       not yet integrated with main programm, not tested
;
; EXAMPLE:
;
;         Horizontal gaussian filter for a small line 
;
;         IDL> g = gaussflt(1.0, 5, 0, /gmf)
;         IDL> print, g
;             0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000
;             0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000
;             0.000000     0.138387    0.0888275   -0.0991522    -0.256124   -0.0991522    0.0888275     0.138387     0.000000
;             0.000000     0.138387    0.0888275   -0.0991522    -0.256124   -0.0991522    0.0888275     0.138387     0.000000
;             0.000000     0.138387    0.0888275   -0.0991522    -0.256124   -0.0991522    0.0888275     0.138387     0.000000
;             0.000000     0.138387    0.0888275   -0.0991522    -0.256124   -0.0991522    0.0888275     0.138387     0.000000
;             0.000000     0.138387    0.0888275   -0.0991522    -0.256124   -0.0991522    0.0888275     0.138387     0.000000
;             0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000
;             0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000     0.000000
;         
; MODIFICATION HISTORY:
;          Written by Andre Stumpf 26-May-2011, stumpf24883@itc.nl
;          based on:  S.Chaudhuri,S.Chatterjee,N.Katz,M.Nelson,M.Goldbaum (1989): Detection of blood vessels in retinal images using two-
;                     dimensionalmatchedfilters,IEEE Trans. Med.Imaging, 8(3), 263–269.
;                     
;                     B. Zhang, L. Zhang, L. Zhang and F. Karray (2010): Retinal vessel extraction by matched filter with first-order 
;                     derivative of Gaussian, Computers in Biology and Medicine, 40(4), 438-445.
;                     MATLAB code available at: http://www4.comp.polyu.edu.hk/~cslzhang/code.htm [25-May-2011]
;-

function gaussflt, sigma, length, theta, GMF=gmf, FDOG=fdog, PLOTG=plotg ;inputs are expected to be double
    ;n_orient=36
    ;theta= (!dpi/n_orient*theta)
    ; calculate the width of the kernel according to choosen sigma and length
    width = ceil(sqrt((6*ceil(sigma)+1)^2 + length^2))
  
    ; assure that the kernel size is uneven
    if width mod 2 eq 0 then width=width+1
     
     ; calculate half the kernel width
     hwidth = (width-1)/2

    ; create arrays with x and y coordinates
      
    x = FIndGen(width)-hwidth
    y = FIndGen(width)-hwidth
    xarr = rebin(x, width, width)
    yarr = rebin(reform(y,1,width), width, width)
    
    ; apply rotation
    xp = (xarr) * cos(theta) - (yarr) * sin(theta)
    yp = (xarr) * sin(theta) + (yarr) * cos(theta)
    yp =rotate(yp,2) ; to match the coordinates with the original Matlab script
    
     
    ; cut off values for the funtion: |x|<=t*sigma and y |y|<=length/2 
    ; bound for x is commonly set to (t=3) * sigma
    index0 = WHERE( ((abs(xp)) GT 3.5*ceil(sigma)) or ((abs(yp)) GT (length)/2) )
    
    ; check if kernel keywords are defined and set kernels for concatenation at the end 
    if n_elements(gmf)+ n_elements(fdog) eq 0 then print, "keyword /GMF or /FDOG must be defined to call this function!"
    g = dblarr(width, width)
    gp = dblarr(width, width)
        
    if keyword_set(gmf)then begin
    
        ; calculate gmf kernel and set it zero beyond the bounds
        g = -exp(-.5D*(xp/sigma)^2)/(sqrt(2*!dpi)*sigma)
        g[index0] = 0      
        ; normalize the mean value of the kernel to zero
        index1 = WHERE(g LT 0, count)
        mean= total(g)/count
        g[index1] = g[index1]-mean
        
        ; plot if specified
        if keyword_set(PLOTG) then ISURFACE, g, xx, yy, color='light blue'
        
    endif
    
    if keyword_set(fdog)then begin
        
        ; calculate fdog kernel and set it zero beyond the bounds
        gp = -exp(-.5D*(xp/sigma)^2)*xp/(sqrt(2*!dpi)*sigma^3)
        gp[index0] = 0 
          
        ; plot if specified
        if keyword_set(PLOTG) then ISURFACE, gp, xx, yy, color='light blue'
        
    endif    
    
    ; store the kernels into an array
    kernel= [[[g]],[[gp]]]
    
return, kernel

end
