;+
; Create ENVI button in the 'FILTER' menu.
;-
pro filter_bright_patches_buttons, ev

  compile_opt idl2
  
  catch, theerror
  if theerror ne 0 then begin
    catch, /cancel
    message = [!error_state.msg]
    void = dialog_message(message, /center, /error)
    return
    
  endif

; select input band, query file for dimension and projection.
  envi_select, fid=band_fid, dims=band_dims, pos=band_pos, title='Select input band for line extraction', /band_only
  if band_fid[0] eq -1 then return
  
  envi_file_query, band_fid, fname=band_fname, data_type=data_type 
  map_info = envi_get_map_info(fid=band_fid)
  band_col = band_dims[2]-band_dims[1]+1
  band_lin = band_dims[4]-band_dims[3]+1
  
; read image into memory
  img = envi_get_data(fid=band_fid, dims=band_dims, pos=band_pos)

;create pointer to the image for display
  imagePtr=ptr_new(img)

  ; Create GUI for selecting input parameters and output image
  tlb=widget_auto_base(title='Top Hat - Brightness filter')
  draw = widget_draw(tlb, XSIZE=band_col, YSIZE=band_lin, uvalue='info', /scroll, X_SCROLL_SIZE=600, Y_SCROLL_SIZE=500)
  
  ; Create draw widget, sensitive to button events and the other widgets for filter parameters
     frame1 = widget_base(tlb, /row, /frame)
    
            radius= widget_sslider(frame1, dt=5, min=1, max=30, value=10, scale=1, xs=[200,4], title='Size of the morphological tophat', uvalue='radius',/auto_manage,/drag)
            local= widget_sslider(frame1, dt=5, min=1, max=255, value=160, scale=1, xs=[200,4], title='Local contrast', uvalue='local',/auto_manage,/drag)
            global= widget_sslider(frame1, dt=5, min=0.1, max=4., value=1., scale=1, xs=[200,4], title='Global contrast [x * STDV above the mean]', uvalue='times',/auto_manage,/drag)
            label=widget_label(tlb, value='Both contrast conditions have to be satisfied for removing the patch.', /align_center)
            
    frame2 = widget_base(tlb, /row, /frame)
    
            resample = widget_toggle(frame2, list=['Off', 'On'], prompt = 'Resampling', uvalue='resample', /auto)
            method = widget_toggle(frame2, list=['Nearest Neighbor', 'Bilinear', 'Cubic'], prompt = 'Method', uvalue='method', /auto)
            factor= widget_sslider(frame2, dt=5, min=0.1, max=30, value=5.0, scale=1, xs=[200,4], title='(Down)Sampling factor', uvalue='factor',/auto_manage,/drag)
            
    output = widget_outf(tlb, prompt='Output Line image', uvalue='out', /auto)
    
  ;make the draw widget active and draw image into it
  widget_control, tlb, /realize
  widget_control, draw, get_value=winID 
  wset, winID
  tvscl, *imagePtr, /order
  
  result = auto_wid_mng(tlb)
  if result.accept eq 0 then return
  
  ; Interprete variables from GUI
  r = result.radius
  local = result.local
  times = result.times
  resample = result.resample
  method = result.method
  factor = result.factor
  if (resample gt 0) then map_info.ps = map_info.ps * result.factor
     
  ; filtering process
  bright=mean(img, /double) + times*stddev(img, /double)                        ;define globally bright
                                                                   
  strucElem = SHIFT(DIST(2*r+1), r, r) LE r                                     ;define locally bright     
  tophat = MORPH_TOPHAT(img, strucElem)                                         ;use tophat to detect locally bright
  indexB = where(img gt bright and tophat gt local, count)                      ;combine the conditions to define bright patches
  indexA = where(img le bright or tophat le local)
  img[indexB]=!Values.F_NAN                                                     ;set bright patches as missing values
  img[indexB]=(smooth(img, 40, /NAN, missing=mean(img, /NAN)))[indexB]
  
  ;-----------------------------------alternative with structuring elements---------------------- 
  
;  radius = 30
;  strucElem = SHIFT(DIST(2*radius+1), radius, radius) LE radius
;  reconstr = morph_close(img, strucElem, /GRAY)
;  
;  img[indexB]=reconstr[indexB]
;  indexB= where(img[indexB] le 0, count)
;  img[indexB]=!Values.F_NAN
;  
;  if count eq 0 then begin
;        print,"No missing value found left after one filtering!"
;              
;    endif else begin
;        
;             while (count gt 0) do begin
;                  radius=radius+5
;                  strucElem = SHIFT(DIST(2*radius+1), radius, radius) LE radius
;                  reconstr = morph_close(img, strucElem, /GRAY)
;                  img[indexB]=reconstr[indexB]
;                  indexB= where(img[indexB] le 0, count)
;                  img[indexB]=!Values.F_NAN
;             endwhile
;    endelse
    
;------------------------------------------------alternative idea with difference between the boundary and the regions  
;  dims = SIZE(img, /DIMENSIONS)
;  threshimg =img
;  
;  threshimg[indexA] = 0
;  threshimg = threshimg gt 0
;  regions = = LABEL_REGION(threshimg, /all_neighbors)
;  image_statistics, img, mask=regions, mean_region=mean, /labeled,
;  
;  test=dilate(regions, strucElem)
;  
;  side = 3  
;  strucElem = DIST(side) LE side
;  dilateImg = DILATE(threshimg, strucElem)
;  regions_bound = LABEL_REGION(dilateImg, /all_neighbors)
;  image_statistics, img, mask=regions, mean=mean, /labeled, 
;  hist = HISTOGRAM(regions)   
  ;-----------------------------------------------------------------------------------------------------------------------------------------
;-----------------------very simple approach for adjusting the local ilumination differences---------------------------------------------------- 
;radius = 20
;;background = SMOOTH(img, radius, /EDGE_TRUNCATE, /NAN) 
;strucElem = SHIFT(DIST(2*radius+1), radius, radius) LE radius
;background = morph_close(img, strucElem, /GRAY)
;img = img - background
 ;----------------------------------------------------------------------------------------------------------------------------------------- 
  

;  threshimg[indexA] = 0
;  threshimg = threshimg gt 0
;  side = 3  
;  strucElem = DIST(side) LE side
;  dilateImg = DILATE(threshimg, strucElem)
;  img[indexB]=!Values.F_NAN  
;  indexA=where(dilateImg gt 0)
  
  
;  img[indexB]=(smooth(img, r, /NAN, missing=mean(img, /NAN), /edge_truncate))[indexB]
  
  
  
;  ;------------------------------------initial algorithm replacing iterativley with median values---------------------------------------------
;  img[indexB]=(median(img, r))[indexB]
;  
;  search_array=img
;  search_array[indexA]=254
;  indexB=where(search_array le 0, count)
;  
;    if count eq 0 then begin
;        print,"No missing value found left after one filtering!"
;              
;    endif else begin
;        r = r+1
;        i=5
;             while (i gt 0 and count gt 0) do begin
;                    img[indexB]=!Values.F_NAN
;                    img[indexB] = (median(img, r))[indexB]        
;                    search_array=img
;                    search_array[indexA]=254
;                    indexB=where(search_array le 0, count)
;                    r = r+1
;                    i=i-1
;             endwhile
;    endelse
;  
;img[indexB]=(smooth(img, r, /NAN, missing=mean(img, /NAN), /edge_truncate))[indexB]           ;replace values that are still missing with the local mean
  
  
  ; resampling
  
  if (resample gt 0) then begin
  
   band_col= band_col*(1/factor)
   band_lin= band_lin*(1/factor)
  
      case method of
        0: img = congrid(img, band_col, band_lin, /center)
        1: img = congrid(img, band_col, band_lin, /center, /interp)
        2: img = congrid(img, band_col, band_lin, /center, cubic=-0.5)
      endcase
     
  endif
  
  ; write description for file header
  description = 'Single band image after filtering bright patches'$
  + '  Filter size='+strcompress(string(r,format='(F10.2)'))$
  + '  Local contrast =' + strcompress(string(local,format='(F10.2)'))$
  + '  Factor for global contrast mean + x * stdv=' + strcompress(string(times,format='(F10.2)'))
    
  ; close file
  openw, out_lun, result.out, /get_lun
  ; write file
  writeu, out_lun, img
      ; close file
  free_lun, out_lun
     
  envi_setup_head, data_type=data_type, fname=result.out, interleave=0, nb = 1, bnames = ['Filtered image'],  ns=band_col, nl=band_lin, offset = 0, descrip=description, map_info=map_info, /open, /write
  
end
  
  
  
  