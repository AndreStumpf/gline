;+
; Create ENVI button in the 'FILTER' menu.
;-

pro gline_define_buttons,ev

  compile_opt idl2
    
  envi_define_menu_button, buttoninfo, value = 'Gaussian Line Extractor', $  
    uvalue = 'Gaussian Line Extractor', event_pro = 'gline', $  
    ref_value = 'Filter', position = 'last' 
    
end

;+
; Main program
;-

pro gline ,ev

  compile_opt idl2
  
  catch, theerror
  if theerror ne 0 then begin
    catch, /cancel
    message = [!error_state.msg]
    void = dialog_message(message, /center, /error)
    return
    
  endif

; select input band, query file for dimension and projection.
  envi_select, fid=band_fid, dims=band_dims, pos=band_pos, title='Select input band for line extraction', /band_only
  if band_fid[0] eq -1 then return
  
  envi_file_query, band_fid, fname=band_fname
  map_info = envi_get_map_info(fid=band_fid)
  band_col = band_dims[2]-band_dims[1]+1
  band_lin = band_dims[4]-band_dims[3]+1
  
  ; read image into memory
  img = double(envi_get_data(fid=band_fid, dims=band_dims, pos=band_pos))  
     
  ;create pointer to the image for display
  imagePtr=ptr_new(img)

  ; Create GUI for selecting input parameters and output image
  tlb=widget_auto_base(title='Gaussian Line Extractor')
  
    ; Create draw widget, sensitive to button events and the other widgets for filter parameters
    draw = widget_draw(tlb, XSIZE=band_col, YSIZE=band_lin, uvalue='info', /button_events, event_pro='draw_line', /scroll, X_SCROLL_SIZE=600, Y_SCROLL_SIZE=500)
    erase = widget_button(tlb, value='Erase Profiles', event_pro='button_event')

    frame1 = widget_base(tlb, /row, /frame)
    
            prefilter = widget_toggle(frame1, list=['Off', 'On'], prompt = 'Bilateral Filtering', uvalue='bilateral_filter', /auto)
            sigmaS= widget_sslider(frame1, dt=5, min=1, max=30, value=10, scale=1, xs=[200,4], title='Filter size [simgaS]', uvalue='sigmaS',/auto_manage,/drag)
            sigmaR= widget_sslider(frame1, dt=5, min=1, max=50, value=8, scale=1, xs=[200,4], title='Filter intensity [simgaR]', uvalue='sigmaR',/auto_manage,/drag)  

    frame2 = widget_base(tlb, /row, /frame)
    
            lcol = widget_base(frame2, /col, /frame)
            rcol = widget_base(frame2, /col, /frame)
              
              llabel=widget_label(lcol, value='Smaller lines', /align_center)
              rlabel=widget_label(rcol, value='Bigger lines', /align_center)
             
            lcol_row1 = widget_base(lcol, /row, /frame)
              param1 = widget_param(lcol_row1, dt=5, field=2, floor=0.1, default=1.0, uvalue='sigma_small', prompt='Gaussian STDV', /auto)
            
            rcol_row1 = widget_base(rcol, /row, /frame)  
              param2 = widget_param(rcol_row1, dt=5, field=2, floor=0.1, default=2.0, uvalue='sigma_large', prompt='Gaussian STDV', /auto)
      
            lcol_row2 = widget_base(lcol, /row, /frame)
              param1 = widget_param(lcol_row2, dt=2, default=5, floor=1, uvalue='length_small', prompt='Length Y [pixel]', /auto)
            
            rcol_row2 = widget_base(rcol, /row, /frame)
              param2 = widget_param(rcol_row2, dt=2, default=10, floor=1, uvalue='length_large', prompt='Length Y [pixel]', /auto)
     
     smoothing= widget_sslider(tlb, dt=5, min=3, max=51, value=3, scale=1, xs=[245,10], title='Size smoothing window [ODD NUMBER of pixel]', uvalue='window_smooth',/auto_manage,/drag)
     orientations = widget_sslider(tlb, dt=5, min=1, max=90, value=12, scale=1, xs=[245,10], title= 'Number of orientations [180/n]', uvalue='n_orient' ,/auto_manage, /drag)
     threshold = widget_sslider(tlb, dt=5, min=1, max=200., value=2., scale=0.1, xs=[245,10], title= 'Constant C for threshold sensitivity', uvalue='Ct' ,/auto_manage, /drag)
   
     output = widget_outf(tlb, prompt='Output Line image', uvalue='out', /auto)
    
  ;make the draw widget active and draw image into it
  widget_control, tlb, /realize
  widget_control, draw, get_value=winID 
  wset, winID
  tvscl, *imagePtr, /order, /NAN

;-----------------------------------------------------------------------------------------------------------------
; create the structure with the variables for the line draw procedure

drawColor = !D.N_Colors-1

   info = { img:img, $            ; The image data.
            winID:winID, $        ; The window index number.
            draw:draw, $          ; The draw widget identifier.
            pixID:-1, $           ; The pixmap identifier.
            xsize:band_col, $     ; The X size of the graphics window.
            ysize:band_lin, $     ; The Y size of the graphics window.
            xstart:-1, $          ; The starting X coordinate of the line.
            ystart:-1, $          ; The starting Y coordinate of the line.
            xvalues:Ptr_New(), $  ; The X coordinates of the free-hand line.
            yvalues:Ptr_New(), $  ; The Y coordinates of the free-hand line.
            buttonUsed:'NONE', $  ; A flag to indicate which button is used.
            drawColor:drawColor } ; The rubberband box color.
            
; write the structure
 widget_control, draw, set_uvalue=info
 
 
; set those variable to common, they are called for the erease button procedure
 common share2, a, b, c
 
 a = winID
 b = img
 c = drawColor
;-----------------------------------------------------------------------------------------------------------------

  result = auto_wid_mng(tlb)
  if result.accept eq 0 then return
  
  ; Interprete variables from GUI
  sigma1= result.sigma_small
  sigma2= result.sigma_large
  length1 =  result.length_small
  length2 =  result.length_large
  smooth_win = result.window_smooth
  n_orient = result.n_orient
  Ct = result.Ct
  bilateral_filter = result.bilateral_filter
  sigmaS = result.sigmaS
  sigmaR = result.sigmaR
  
; Pre-proscessing with bilateral filter if box was checked 
if (bilateral_filter gt 0) then begin
   img = bilateral(img, sigmaS, sigmaR, /EDGE_TRUNCATE)
endif

;Matched Filtering------------------------------------------------------------------------------ 

tic=systime(1)

smooth_win=ceil(6*sigma1)
;initiate first orientation
i=0
theta= (!dpi/n_orient*i)
kernel=(gaussflt(sigma1, length1, theta, /gmf, /fdog))
img_gmf = convol(img, kernel[*,*,0], /edge_zero, /NAN)
img_fdog = abs(smooth(convol(img, kernel[*,*,1], /edge_zero, /NAN), smooth_win, /edge_truncate, /NAN))

for i=1, n_orient do begin
    theta= ((!dpi/n_orient)*i)
    kernel = (gaussflt(sigma1, length1, theta, /gmf, /fdog))
    img_gmf_temp = convol(img, kernel[*,*,0], /edge_zero, /NAN)
    index =  where(img_gmf_temp gt img_gmf)
    img_gmf[index] = img_gmf_temp[index]
    print, img_gmf_temp[6, 9]
    img_fdog[index] = (abs(smooth(convol(img, kernel[*,*,1], /edge_zero, /NAN), smooth_win, /edge_truncate, /NAN)))[index]
    print, 'Probing orientation number' + STRING(i)
endfor    
    
print, systime(1)-tic    

; threshold GMF response at 0
index0=where(img_gmf lt 0)
img_gmf[index0]=0


;    ;--------------------apply threshold option 1------------------- 
;    ; normalize smoothed FDOG
;    img_fdog_norm = (img_fdog - (min(img_fdog))) / ((max(img_fdog))-(min(img_fdog)))  
;    ; calculate array with threshold at each location based on mean, Ct and FDOG response
;    T = (1+img_fdog_norm)*(mean(img_gmf))*Ct
;    ; apply threshold array
;    indexT = where(img_gmf lt T)
;    img_gmf[indexT]=0
          
    ;--------------------apply threshold option 2-------------------   
    ; subtract FDOG response from GMF response
    ;(scope_varfetch('img_gmf', /enter, level=1)) = img_gmf
    ;(scope_varfetch('img_fdog', /enter, level=1)) = img_fdog
    
    img_gmf = img_gmf - ct * img_fdog
    thresh = mean(img_gmf, /NAN) + 2*stddev(img_gmf, /NAN)
    indexT = where(img_gmf lt thresh)
    img_gmf[indexT]=0  


; ---------------------------------------------------------------morphological post-processing----------------------------------------------------
; convert it into a binary response 
binarr=img_gmf gt 0
binarr_orig = binarr

; create arrays to store matches
matches=bytarr(band_col, band_lin)
matches_store=bytarr(band_col, band_lin)
;----------------------------------------------------


; horizontal gaps
hit = [[0b,0b,0b],$  
       [1b,0b,1b], $  
       [0b,0b,0b]]
       
miss = [[1b,1b,1b], $  
      [0b,1b,0b], $  
      [1b,1b,1b]]

matches = matches + (morph_hitormiss(binarr, hit, miss))

; vertical gaps
miss = rotate(miss,1)
hit = rotate(hit,1)
matches = matches + (morph_hitormiss(binarr, hit, miss))

;add to binarr
binarr = binarr + matches
binarr = binarr gt 0
matches_store=matches_store+matches
matches=bytarr(band_col, band_lin)

;----------------------------------------------------


; diagonal gaps
hit =[[0b,0b,0b,0b,0b],$  
       [0b,0b,0b,1b,0b],$  
       [0b,0b,0b,0b,0b],$  
       [0b,1b,0b,0b,0b],$  
       [0b,0b,0b,0b,0b]]

              
miss = [[0b,0b,1b,0b,0b],$  
       [0b,1b,1b,0b,0b],$  
       [1b,1b,1b,1b,1b],$  
       [0b,0b,1b,1b,0b],$  
       [0b,0b,1b,0b,0b]]
      

matches= matches + (morph_hitormiss(binarr, hit, miss))

; the other diagonal gaps 
miss =rotate(miss,1)
hit = rotate(hit,1)     
matches= matches + (morph_hitormiss(binarr, hit, miss))

;add to binarr
binarr = binarr + matches
binarr = binarr gt 0
matches_store=matches_store+matches
matches=bytarr(band_col, band_lin)
;----------------------------------------------------


; asymetric gaps
hit = [[1b,0b,0b],$  
       [0b,0b,1b], $  
       [0b,0b,0b]]

miss = [[0b,1b,1b], $  
      [1b,1b,0b], $  
      [1b,1b,0b]]

  for i=0, 7 do begin        
        missi=rotate(miss, i)
        hiti=rotate(hit, i)
        matches= matches + (morph_hitormiss(binarr, hiti, missi))

  end

;add to binarr
binarr = binarr + matches
binarr = binarr gt 0
matches_store=matches_store+matches
matches=bytarr(band_col, band_lin)
;----------------------------------------------------


; asymetric steps
hit = [[0b,1b,1b],$  
       [1b,0b,0b], $  
       [0b,0b,0b]]

miss = [[1b,0b,0b], $  
      [0b,1b,1b], $  
      [1b,1b,1b]]
  
  for i=0, 7 do begin
        missi=rotate(miss, i)
        hiti=rotate(hit, i)
        matches= matches + (morph_hitormiss(binarr, hiti, missi))  
  end 

;add to binarr
binarr = binarr + matches
binarr = binarr gt 0
matches_store=matches_store+matches
matches=bytarr(band_col, band_lin)
;----------------------------------------------------


; symetric steps
hit = [[0b,0b,0b],$  
       [1b,0b,0b], $  
       [0b,1b,0b]]

miss = [[1b,1b,1b], $  
      [0b,1b,1b], $  
      [1b,0b,1b]]

  for i=0, 3 do begin        
        missi=rotate(miss, i)
        hiti=rotate(hit, i)
        matches= matches + (morph_hitormiss(binarr, hiti, missi))  
  end

;add to binarr
binarr = binarr + matches
binarr = binarr gt 0
matches_store=matches_store+matches
matches_store=matches_store gt 0
;----------------------------------------------------

img_gmf=[ [[binarr_orig]],[[binarr]],[[matches_store]] ]

; write description for file header
  description = 'Line image derrived from a Gaussian Line Extractor'$
  + '  Kernel Sigma='+strcompress(string(sigma1,format='(F10.2)'))$
  + '  Kernel Length=' + strcompress(string(length1,format='(F10.2)'))$
  + '  Width of Mean Filter='+strcompress(string(smooth_win,format='(F10.2)'))$
  + '  Number of Orientations='+strcompress(string(n_orient,format='(F10.2)'))$
  + '  Threshold C='+strcompress(string(Ct,format='(F10.2)'))
    
  ; close file
  openw, out_lun, result.out, /get_lun
  ; write file
  writeu, out_lun, img_gmf
  ; close file
  free_lun, out_lun
     
  envi_setup_head, data_type=1, fname=result.out, interleave=0, nb = 3, bnames = ['Linear structures raw', 'Linear structures gaps filled', 'Gaps'],  ns=band_col, nl=band_lin, offset = 0, descrip=description, map_info=map_info, /open, /write
end

;-------------------------------------------------------------------------------------------------------------------------------------
pro draw_line, event       

; handle only press, release and motion events
if event.type gt 2 then return

; legend for event.type
; 0 Button press
; 1 Button release
; 2 Motion

; legend for event.press
;1 Left
;2 Middle
;4 Right 

widget_control, event.id, get_uvalue=info, /No_Copy

if event.type eq 0 and event.press eq 1 then begin
 
            ; Turn motion events on for the draw widget.
            widget_control, info.draw, draw_motion_events=1   
            
                     Window, /Free, /Pixmap, XSize=info.xsize, YSize=info.ysize
                 info.pixID = !D.Window
                 Device, Copy=[0, 0, info.xsize, info.ysize, 0, 0, info.winID]
        
                    ; Initialize the starting coordinates of the line.
        
                    info.xstart = event.x
                    info.ystart = event.y
            
endif

; left button released
if event.type eq 1 and event.press ne 4 then begin

         WSet, info.winID
         Device, Copy=[0, 0, info.xsize, info.ysize, 0, 0, info.pixID]
         WDelete, info.pixID

         ; Turn draw motion events off. Clear events queued for widget.
         widget_control, info.draw, Draw_Motion_Events=0, Clear_Events=1
         
         PlotS, [info.xstart, event.x], [info.ystart, event.y], $
              /Device, Color=info.drawColor
                 
                  ;get the the profile 
                  x1= info.xstart
                  x2= event.x 
                  y1= info.ysize - info.ystart  ; because displayed is set /order
                  y2= info.ysize - event.y      ; because displayed is set /order
                  
                  nPoints = ABS(x2-x1) > ABS(y2-y1)
                  
                  xloc = x1 + (x2 - x1) * Findgen(nPoints) / (nPoints)
                  yloc = y1 + (y2 - y1) * Findgen(nPoints) / (nPoints)  
                  profile = Interpolate(info.img, round(xloc), round(yloc))
                  
                    ; vector with x-values (pixel numbers)
                    xi=findgen(nPoints)
                    fit = GAUSSFIT(xi, profile, coeff, CHISQ=chisquare, YERROR=SE, NTERMS=4)  
                    
                    errorbar = REPLICATE( SE, npoints ) 
                    
                    ;calaculate standard deviation of the intensity values along in the profile
                    ; when the profile is drawn on homogenous background this value can be used to adjust sigma(range) of the bilateral filter
                    sigmaR = stddev(profile, /double)
                    
                    
                  iplot, profile, thick=2.0, name='Profile', /INSERT_LEGEND, VIEW_TITLE='Current Profile', COLOR = [150, 150, 150],  BACKGROUND_COLOR=[220, 220, 220]     
                  iplot, fit, overplot=1, thick=2.0, yerror=errorbar, /yerrorbar, name='Gaussian Fit', linestyle=2, /INSERT_LEGEND, TITLE = 'sigma=' + strcompress(string(coeff[2],format='(F10.2)'))+'   Chi squ.='+strcompress(string(chisquare,format='(F10.2)')) + '   sigmaR=' + strcompress(string(sigmaR,format='(F10.2)'))             
                  
                  
; Reinitialize the line starting coordinates.

            info.xstart = -1
            info.ystart = -1    
            
endif
         
if event.type eq 2 then begin
         
         ;Here is where the actual line is drawn and erased First, erase the last line.

         wset, info.winID
         device, Copy=[0, 0, info.xsize, info.ysize, 0, 0, info.pixID]       
           
         plots, [info.xstart, event.x], [info.ystart, event.y], /Device, Color=info.drawColor
         
         print, 'also motion is considered'  
endif
  
Widget_Control, event.ID, Set_UValue=info, /No_Copy  

end     
;+-------------------------------------------------------------------------------------------------------------------------------------    
 PRO button_event, event

   widget_control, event.id, get_value=thisButtonValue
 
; common share2, a, b, c 
; winID = a 
; img = b
; drawColor = c
    
    
 print, winID
 print, (n_elements(img))
 print, drawColor
  
   if (thisButtonValue = 'Erase Profiles') then begin

         wset, winID
         tvscl, img, top=drawColor-1, /order, /NAN
   
   endif 
         
   end
;------------------------------------------------------------------
