function file_open, fname, event

envi_open_file, fname, r_fid=fid
envi_select, fid=fid,dims=dims,pos=pos,title = 'select image file'
envi_file_query, fid, ns=ns, nl=nl, nb=nb, fname=fname


; create array
image = fltarr(ns,nl, nb)

  for i=0, nb-1 do begin
    inBand=envi_get_data(fid=fid,dims=dims,pos=i)       
    image[*,*,i]=inBand
  endfor
  
return, image

end