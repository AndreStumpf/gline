# Gline #

An ENVI-IDL module for line detection with Gaussian matched filters. Slight modification of the routine described in:

Stumpf, A., Malet, J.-P., Kerle, N., Niethammer, U., & Rothmund, S. (2013). Image-based mapping of surface fissures for the investigation of landslide dynamics. Geomorphology, 186(0), 12-27. doi: http://dx.doi.org/10.1016/j.geomorph.2012.12.010


### Getting started ###

* The module GUI requieres the classical ENVI-IDL interface.
* The main routine is gline.pro
* Accepted inputs are single band images

![1_select.PNG](https://bitbucket.org/repo/4EBdMk/images/314614254-1_select.PNG)

* The main window allows probing the image to determine the scale (sigma) of the line detection
![2_probe.PNG](https://bitbucket.org/repo/4EBdMk/images/3835253477-2_probe.PNG)
* The main parameters are the sigma of the Gaussian kernel (i.e. the width of the line), the lenght of the kernel (i.e. the length of aproximately straight segments), the size of the smoothing window for the FDOG response (see paper for details), the number of probed orientations (only small gains for > 12), and the sensitivity threshold C (i.e. smaller > more detections, larger > fewer detections). Only the first sigma (smaller lines) is currently considered and parameters set in 'bigger lines' are ignored.
![3_parameters.PNG](https://bitbucket.org/repo/4EBdMk/images/334141026-3_parameters.PNG)

* The resulting image comprises three bands of which the first one holds the final result after filling gaps with morphological filters
![4_result.PNG](https://bitbucket.org/repo/4EBdMk/images/1898028997-4_result.PNG)

### Some general hints and limitations ###
The routine runs rather quick at small kernel sizes (sigma 1-3) whereas runtime increases considerably with sigmas larger than that. If the features you want to detect exceed 5-6 sigma you might want to consider to downsample your image first. The morphological filter fills gaps in small pixel neighborhoods but cannot interpolate large gaps among line segments.

The bilateral post-processing is functional and was intended to reduce false positives on very noisy images. However, in most cases it does not seem to be beneficial. It is off by default to avoid additional parameters and computation.


### Contact ###

* André Stumpf, andre.stumpf@unistra.fr