pro gaussline, image_folder, sigma, length, n_orient, Ct, output_raw=output_raw, normalize=normalize

; :Description: An IDL procedure implementing line detection in images with Gaussian Matched Filters
;  as described in Stumpf, André, et al. "Image-based mapping of surface fissures for the investigation
;  of landslide dynamics." Geomorphology 186 (2013): 12-27.
;  The output files will be written next to the input files with the suffix '_gline_reponse'
;  
; :Author:  Andre Stumpf, andre.stumpf@unistra.fr
; 
; :Params: 
;     image_folder : in, required, type=string
;        folder containing the input images in GeoTiff format 
;     sigma : in, required, type=float 
;        standard deviation of Gaussian kernel
;     length : in, required, type=float
;        length of the Gaussian kernel
;     n_orient : in, required, type=byte
;        number of orientations the will be tested
; 
; :Keywords: 
;     output_raw : in, type=string
;       if set output contains the raw filter response
;     normalize : in, type=string
;       if set raw filter response is normalized to a range between 0 and 1
;     
;       
; :Examples:       
; image_folder = 'C:\Users\stumpf\Documents\TestData'
; sigma = 0.6
; length = 6
; n_orient = 12
; Ct = 1.5
; gaussline, image_folder, sigma, length, n_orient, Ct, /output_raw, /normalize

; get all tif files
datafiles = file_search(image_folder, '*.tif', /fold_case, /fully_qualify_path, count=n_file)

; check if files have been found
if (n_file eq 0) then message, 'No .tif files found in the specified folder. Check path or file ending'

foreach file, datafiles do begin
  print, 'Processing ', file
  
  ; Get information about the IDL-created variable that 
  imp = query_tiff(file, file_info)
  band_col = file_info.dimensions[0]
  band_lin = file_info.dimensions[1]
  ; HELP, file_info, /STRUCTURE
  
  ; load data and store geoinformation to GeoKeys
  iopen, file, img, GEOTIFF=GeoKeys
  ; HELP, GeoKeys, /STRUCTURE
  
  ; check if multi-band
  if (file_info.channels gt 1) then begin
    print, 'Image contains more than one channel...using only the first 
    img = reform(img[0,*,*])
  endif
  
  ; concetenate output filename
  outfile_name = file_dirname(file,  /MARK_DIRECTORY) + file_basename(file, '.tif') + '_gline_response.tif'
   
  ; convert image to double
  (scope_varfetch('img', /enter, level=1)) = img
  img = double(img)
  
  if keyword_set(normalize) then begin 
    img = (img - min(img)) / max(img)
  endif
    
  ;Matched Filtering------------------------------------------------------------------------------ 
  tic=systime(1)
  
  smooth_win=ceil(6*sigma)
  ;initiate first orientation
  i=0
  theta= (!dpi/n_orient*i)
  kernel=(gaussflt(sigma, length, theta, /gmf, /fdog))
  img_gmf = convol(img, kernel[*,*,0], /edge_zero, /NAN)
  img_fdog = abs(smooth(convol(img, kernel[*,*,1], /edge_zero, /NAN), smooth_win, /edge_truncate, /NAN))
  
  for i=1, n_orient do begin
      theta= ((!dpi/n_orient)*i)
      kernel = (gaussflt(sigma, length, theta, /gmf, /fdog))
      img_gmf_temp = convol(img, kernel[*,*,0], /edge_zero, /NAN)
      index =  where(img_gmf_temp gt img_gmf)
      img_gmf[index] = img_gmf_temp[index]
      img_fdog[index] = (abs(smooth(convol(img, kernel[*,*,1], /edge_zero, /NAN), smooth_win, /edge_truncate, /NAN)))[index]
      print, 'Probing orientation number' + string(i)
  endfor
  
  print, systime(1)-tic    
  
  ; threshold GMF response at 0
  index0=where(img_gmf lt 0)
  img_gmf[index0]=0
  
  ;    ;--------------------apply threshold option 1------------------- 
  ;    ; normalize smoothed FDOG
  ;    img_fdog_norm = (img_fdog - (min(img_fdog))) / ((max(img_fdog))-(min(img_fdog)))  
  ;    ; calculate array with threshold at each location based on mean, Ct and FDOG response
  ;    T = (1+img_fdog_norm)*(mean(img_gmf))*Ct
  ;    ; apply threshold array
  ;    indexT = where(img_gmf lt T)
  ;    img_gmf[indexT]=0
            
      ;--------------------apply threshold option 2-------------------   
      ; subtract FDOG response from GMF response
        
      img_gmf = img_gmf - ct * img_fdog
      thresh = mean(img_gmf, /NAN) + 2*stddev(img_gmf, /NAN)
      indexT = where(img_gmf lt thresh)
      img_gmf[indexT]=0 
      
      ;(scope_varfetch('img_gmf', /enter, level=1)) = img_gmf
      ;(scope_varfetch('img_fdog', /enter, level=1)) = img_fdog
      
  ; ---------------------------------------------------------------morphological post-processing----------------------------------------------------
  ; convert it into a binary response 
  binarr=img_gmf gt 0
  binarr_orig = binarr
  
  ; create arrays to store matches
  matches=bytarr(band_col, band_lin)
  matches_store=bytarr(band_col, band_lin)
  ;----------------------------------------------------
   
  ; horizontal gaps
  hit = [[0b,0b,0b],$  
         [1b,0b,1b], $  
         [0b,0b,0b]]
         
  miss = [[1b,1b,1b], $  
        [0b,1b,0b], $  
        [1b,1b,1b]]
  
  matches = matches + (morph_hitormiss(binarr, hit, miss))
  
  ; vertical gaps
  miss = rotate(miss,1)
  hit = rotate(hit,1)
  matches = matches + (morph_hitormiss(binarr, hit, miss))
  
  ;add to binarr
  binarr = binarr + matches
  binarr = binarr gt 0
  matches_store=matches_store+matches
  matches=bytarr(band_col, band_lin)
  
  ;----------------------------------------------------
  
  ; diagonal gaps
  hit =[[0b,0b,0b,0b,0b],$  
         [0b,0b,0b,1b,0b],$  
         [0b,0b,0b,0b,0b],$  
         [0b,1b,0b,0b,0b],$  
         [0b,0b,0b,0b,0b]]
  
                
  miss = [[0b,0b,1b,0b,0b],$  
         [0b,1b,1b,0b,0b],$  
         [1b,1b,1b,1b,1b],$  
         [0b,0b,1b,1b,0b],$  
         [0b,0b,1b,0b,0b]]
        
  
  matches= matches + (morph_hitormiss(binarr, hit, miss))
  
  ; the other diagonal gaps 
  miss =rotate(miss,1)
  hit = rotate(hit,1)     
  matches= matches + (morph_hitormiss(binarr, hit, miss))
  
  ;add to binarr
  binarr = binarr + matches
  binarr = binarr gt 0
  matches_store=matches_store+matches
  matches=bytarr(band_col, band_lin)
  ;----------------------------------------------------
  
  ; asymetric gaps
  hit = [[1b,0b,0b],$  
         [0b,0b,1b], $  
         [0b,0b,0b]]
  
  miss = [[0b,1b,1b], $  
        [1b,1b,0b], $  
        [1b,1b,0b]]
  
    for i=0, 7 do begin        
          missi=rotate(miss, i)
          hiti=rotate(hit, i)
          matches= matches + (morph_hitormiss(binarr, hiti, missi))
  
    end
  
  ;add to binarr
  binarr = binarr + matches
  binarr = binarr gt 0
  matches_store=matches_store+matches
  matches=bytarr(band_col, band_lin)
  ;----------------------------------------------------
  
  ; asymetric steps
  hit = [[0b,1b,1b],$  
         [1b,0b,0b], $  
         [0b,0b,0b]]
  
  miss = [[1b,0b,0b], $  
        [0b,1b,1b], $  
        [1b,1b,1b]]
    
    for i=0, 7 do begin
          missi=rotate(miss, i)
          hiti=rotate(hit, i)
          matches= matches + (morph_hitormiss(binarr, hiti, missi))  
    end 
  
  ;add to binarr
  binarr = binarr + matches
  binarr = binarr gt 0
  matches_store=matches_store+matches
  matches=bytarr(band_col, band_lin)
  ;----------------------------------------------------
  
  ; symetric steps
  hit = [[0b,0b,0b],$  
         [1b,0b,0b], $  
         [0b,1b,0b]]
  
  miss = [[1b,1b,1b], $  
        [0b,1b,1b], $  
        [1b,0b,1b]]
  
    for i=0, 3 do begin        
          missi=rotate(miss, i)
          hiti=rotate(hit, i)
          matches= matches + (morph_hitormiss(binarr, hiti, missi))  
    end
  
  ;add to binarr
  binarr = binarr + matches
  binarr = binarr gt 0
  matches_store=matches_store+matches
  matches_store=matches_store gt 0
  ;----------------------------------------------------
  
  (scope_varfetch('img_gmf', /enter, level=1)) = img_gmf
  (scope_varfetch('binarr', /enter, level=1)) = binarr
  
  if keyword_set(normalize) then begin
    img_gmf = (img_gmf - min(img_gmf)) / max(img_gmf)
  endif
     
  
  binarr = reverse(binarr,2)
  img_gmf =reverse(img_gmf,2)
  
  ; concatenate output
  if keyword_set(output_raw) then begin
    
    print, 'Writing raw filter response to the second band ...'
    img_out=[ [[binarr]],[[img_gmf]] ]
  
  endif else begin
    
    img_out=binarr
  
  endelse
  
  ; convert to band-interleaved pixel-by-bixel
  img_out = transpose(img_out, [2,0,1])
  
  ; write description for file header
    description = 'Line image derrived from a Gaussian Line Extractor'$
    + '  Kernel Sigma='+strcompress(string(sigma,format='(F10.2)'))$
    + '  Kernel Length=' + strcompress(string(length,format='(F10.2)'))$
    + '  Width of Mean Filter='+strcompress(string(smooth_win,format='(F10.2)'))$
    + '  Number of Orientations='+strcompress(string(n_orient,format='(F10.2)'))$
    + '  Threshold C='+strcompress(string(Ct,format='(F10.2)'))
  
  ; write output
  print, 'Writing output file ' + outfile_name
  write_tiff, outfile_name, img_out, /float, geotiff=GeoKeys, description=description
   
 endforeach
 
 end
